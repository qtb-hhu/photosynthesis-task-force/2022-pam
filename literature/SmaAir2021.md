---
title: Spectrofluorometric Insights into the Application of PAM Fluorometry in Photosynthetic Research
---

# Sma-Air 2021

[DOI](https://doi.org/10.1111/php.13413)
